package br.ucsal.bes2018.poo.restaurante.domain;

import br.ucsal.bes2018.poo.restaurante.enums.SituacaoMesa;

public class Restaurante {
	private int refrigerante;
	private int cerveja;
	private int vinho;
	private int mesa;
	private int numMesa;
	private int capacidade;
	private SituacaoMesa situacao;
	private int pf;
	private int buffet;
	private int rodizio;

	public int getNumMesa() {
		return numMesa;
	}

	public void setNumMesa(int numMesa) {
		this.numMesa = numMesa;
	}

	public int getMesa() {
		return mesa;
	}

	public void setMesa(int mesa) {
		this.mesa = mesa;
	}

	public SituacaoMesa getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoMesa situacao) {
		this.situacao = situacao;
	}

	public int getCapacidade() {
		return capacidade;
	}

	public void setCapacidade(int capacidade) {
		this.capacidade = capacidade;
	}
	public Restaurante(int mesaCad, int capacidadeMesa, SituacaoMesa situacao) {
		this.capacidade = capacidadeMesa;
		this.numMesa = mesaCad;
	}


	public Restaurante(int qntRefrigerante, int qntCerveja, int qntVinho, int qntPf, int qntBuffet, int qntRodizio,
			SituacaoMesa situacao, int escMesa, int capacidadeMesa) {
		this.refrigerante = qntRefrigerante;
		this.cerveja = qntCerveja;
		this.vinho = qntVinho;
		this.pf = qntPf;
		this.mesa = escMesa;
		this.buffet = qntBuffet;
		this.rodizio = qntRodizio;
		this.situacao = situacao;
		this.capacidade = capacidadeMesa;
	}

	
	public int getRefrigerante() {
		return refrigerante;
	}

	public void setRefrigerante(int refrigerante) {
		this.refrigerante = refrigerante;
	}

	public int getCerveja() {
		return cerveja;
	}

	public void setCerveja(int cerveja) {
		this.cerveja = cerveja;
	}

	public int getVinho() {
		return vinho;
	}

	public void setVinho(int vinho) {
		this.vinho = vinho;
	}

	public int getPf() {
		return pf;
	}

	public void setPf(int pf) {
		this.pf = pf;
	}

	public int getBuffet() {
		return buffet;
	}

	public void setBuffet(int buffet) {
		this.buffet = buffet;
	}

	public int getRodizio() {
		return rodizio;
	}

	public void setRodizio(int rodizio) {
		this.rodizio = rodizio;
	}
}
