package br.ucsal.bes2018.poo.restaurante.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes2018.poo.restaurante.domain.Restaurante;

public class RestauranteDAO {
	public static List<Restaurante> mesas = new ArrayList<Restaurante>();

	public static void addMesa(Restaurante mesa) {
		mesas.add(mesa);

	}

	public static List<Restaurante> todasMesas() {
		return mesas;
	}
}
