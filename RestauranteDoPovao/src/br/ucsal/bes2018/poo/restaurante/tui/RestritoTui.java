package br.ucsal.bes2018.poo.restaurante.tui;

import java.util.List;

import br.ucsal.bes2018.poo.restaurante.business.RestauranteBO;
import br.ucsal.bes2018.poo.restaurante.domain.Restaurante;
import br.ucsal.bes2018.poo.restaurante.enums.SituacaoMesa;
import br.ucsal.bes2018.poo.restaurante.persistence.RestauranteDAO;

public class RestritoTui {
	public static Integer capacidade;
	private static int table;
	

	public static void cadastroMesa() {

		System.out.println("Informe a capacidade dessa mesa: ");
		capacidade = MenuTui.sc.nextInt();

		table++;
		SituacaoMesa situacao = SituacaoMesa.LIVRE;
		Restaurante mesa = new Restaurante(table, capacidade, situacao);
		if (RestauranteBO.validarMesa(mesa) == null) {
			System.out.println("Mesa Nova Cadastrada! ");
			

		} else {
			System.out.println(RestauranteBO.validarMesa(mesa));
		}

		System.out.println("Deseja Voltar: ");
		MenuTui.voltar = MenuTui.sc.next();
	}

	public static void mostrarMesas() {
		List<Restaurante> mesas = RestauranteDAO.todasMesas();
		if (mesas.size() == 0) {
			System.out.println("SEM MESAS DISPON�VEIS!");
			System.out.println("Deseja Voltar: ");
			MenuTui.voltar = MenuTui.sc.next();
		}
		for (Restaurante mesa : mesas) {
			System.out.println("NUMERO MESA: " + mesa.getNumMesa());
			System.out.println("CAPACIDADE MESA: " + mesa.getCapacidade());
			System.out.println("A MESA EST�: " + mesa.getSituacao());
			System.out.println();
		}
		System.out.println("Deseja Voltar: ");
		System.out.println();
		MenuTui.voltar = MenuTui.sc.next();
	}
}
