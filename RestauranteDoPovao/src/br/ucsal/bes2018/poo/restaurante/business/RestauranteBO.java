package br.ucsal.bes2018.poo.restaurante.business;

import br.ucsal.bes2018.poo.restaurante.domain.Restaurante;
import br.ucsal.bes2018.poo.restaurante.persistence.RestauranteDAO;

public class RestauranteBO {
	static int LIM_CAPACIDADE = 5;
 
	RestauranteBO restauranteBo = new RestauranteBO();
	
	public static String validarMesa(Restaurante mesa) {
		if(validar(mesa)== null) {
			RestauranteDAO.addMesa(mesa);	
			return null;
		}
		return validar(mesa);
	}
	public static String validar(Restaurante mesa) {
		if(mesa.getCapacidade() > LIM_CAPACIDADE) {
			return "ERRO, A cima do limite da capacidade";	
		}
		return null;
	}
}
