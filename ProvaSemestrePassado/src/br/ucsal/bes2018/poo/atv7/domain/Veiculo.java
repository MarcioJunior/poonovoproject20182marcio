package br.ucsal.bes2018.poo.atv7.domain;

import br.ucsal.bes2018.poo.atv7.enums.TipoVeicular;

public class Veiculo {
	TipoVeicular tipoVeiculo;
	double valorVeiculo;
	String placaVeiculo;
	long anoFabricacao;

	public double getValorVeiculo() {
		return valorVeiculo;
	}

	public void setValorVeiculo(double valor) {
		this.valorVeiculo = valor;
	}

	public TipoVeicular getTipoVeiculo() {
		return tipoVeiculo;
	}

	public void setTipoVeiculo(TipoVeicular tipo) {
		this.tipoVeiculo = tipo;
	}

	public String getPlacaVeiculo() {
		return placaVeiculo;
	}

	public void setPlacaVeiculo(String placa) {
		this.placaVeiculo = placa;
	}

	public long getAnoFabricacao() {
		return anoFabricacao;
	}

	public void setAnoFabricacao(long ano) {
		this.anoFabricacao = ano;
	}

}
