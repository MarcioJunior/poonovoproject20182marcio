package br.ucsall.bes20182.poo.lista11;

import java.util.List;

public class Carros extends Veiculos {
	public static  List<Veiculos> carros;
	private Integer quantPortas;

	
	public Carros(String placa, String anoFab, Integer valor, Integer quantPortas) {
		super(placa, anoFab, valor);
		this.quantPortas = quantPortas;
	}
	public Integer getQuantPortas() {
		return quantPortas;
	}

	public void setQuantPortas(Integer quantPortas) {
		this.quantPortas = quantPortas;
	}


	@Override
	public String toString() {
		return "Carros [Placa= " + placa + ", Ano de Fabricação= " + anoFab + ", Valor= R$" + valor + ", Quatidade de Portas= " + quantPortas + "]";
	}
	public void incluir(Veiculos carro) {
		
		carros.add(carro);
	}

}
