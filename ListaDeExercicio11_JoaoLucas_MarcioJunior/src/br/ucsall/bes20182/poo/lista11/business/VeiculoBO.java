package br.ucsall.bes20182.poo.lista11.business;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.ucsall.bes20182.poo.lista11.Veiculos;

public class VeiculoBO {
	public static List<Veiculos> veiculos = new ArrayList<>();
	
	 public static void incluir(Veiculos veiculo){
	 veiculos.add(veiculo);
		
	 }
	 
	public static void listar(){// ordem crescente por placa 
		Collections.sort(veiculos);
		 for (Veiculos veiculo : veiculos)
				System.out.println(veiculo + "\n");
	 }
}
