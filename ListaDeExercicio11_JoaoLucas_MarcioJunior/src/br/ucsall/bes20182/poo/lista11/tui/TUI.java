package br.ucsall.bes20182.poo.lista11.tui;

import br.ucsall.bes20182.poo.lista11.Caminhoes;
import br.ucsall.bes20182.poo.lista11.Carros;
import br.ucsall.bes20182.poo.lista11.Motos;
import br.ucsall.bes20182.poo.lista11.Veiculos;
import br.ucsall.bes20182.poo.lista11.business.VeiculoBO;
import br.ucsall.bes20182.poo.lista11.enums.TipoCarga;
import br.ucsall.bes20182.poo.lista11.enums.TipoMoto;

public class TUI {

	public static void main(String[] args) {
		
		Carros carro1 = new Carros("OAB2018","2017",26000,3);
		Carros carro2 = new Carros("bes1233","2007",29000,4);
		Caminhoes caminhao1 = new Caminhoes("ARD4562","2015",60000,6,TipoCarga.G�S);
		Caminhoes caminhao2 = new Caminhoes("HGJS","2010",40000,6,TipoCarga.LIQUIDO);
		Motos moto1 = new Motos("VIV8952","2045",80000,TipoMoto.CIDADE);
		Motos moto2 = new Motos("ANE9623","2055",900000,TipoMoto.CAMPO);
		
		
		VeiculoBO.incluir(carro1);
		VeiculoBO.incluir(carro2);
		VeiculoBO.incluir(caminhao1);
		VeiculoBO.incluir(caminhao2);
		VeiculoBO.incluir(moto1);
		VeiculoBO.incluir(moto2);
		VeiculoBO.listar();
		
		

	}

}
