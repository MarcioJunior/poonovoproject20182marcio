package br.ucsall.bes20182.poo.lista11;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class Veiculos implements Comparable<Veiculos>{
	
	protected String placa;

	protected String anoFab;

	protected Integer valor;

	public Veiculos(String placa, String anoFab, Integer valor) {
		super();
		this.placa = placa;
		this.anoFab = anoFab;
		this.valor = valor;
	}


	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getAnoFab() {
		return anoFab;
	}

	public void setAnoFab(String anoFab) {
		this.anoFab = anoFab;
	}

	public Integer getValor() {
		return valor;
	}

	public void setValor(Integer valor) {
		this.valor = valor;
	}
	 
	 @Override
	public String toString() {
		return ", placa=" + placa + ", anoFab=" + anoFab + ", valor=" + valor + "]";
	}

	public int compareTo(Veiculos veiculo) {
		return this.placa.compareToIgnoreCase(veiculo.placa);
	}

}
