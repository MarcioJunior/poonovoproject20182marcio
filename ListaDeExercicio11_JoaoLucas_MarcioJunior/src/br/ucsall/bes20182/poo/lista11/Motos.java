package br.ucsall.bes20182.poo.lista11;

import java.util.List;

import br.ucsall.bes20182.poo.lista11.enums.TipoMoto;

public class Motos extends Veiculos {

	private TipoMoto tipoMoto;
	public static List<Veiculos> motos;

	public Motos(String placa, String anoFab, Integer valor, TipoMoto tipoMoto) {
		super(placa, anoFab, valor);
		this.tipoMoto = tipoMoto;
	}

	public TipoMoto getTipoMoto() {
		return tipoMoto;
	}

	public void setTipoMoto(TipoMoto tipoMoto) {
		this.tipoMoto = tipoMoto;
	}

	public void incluir(Veiculos moto) {
     motos.add(moto);
	}

	@Override
	public String toString() {
		return "Motos [Placa= " + placa + ", Ano de Fabricação= " + anoFab + ", Valor= R$" + valor +", Tipo Moto= " + tipoMoto + "]";
	}

}
