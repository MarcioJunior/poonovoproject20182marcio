package br.ucsall.bes20182.poo.lista11;

import java.util.List;

import br.ucsall.bes20182.poo.lista11.enums.TipoCarga;

public class Caminhoes extends Veiculos {

	private Integer quantEixos;
	public static  List<Veiculos> caminhoes;
	

	private TipoCarga tipoCarga;


	public Caminhoes(String placa, String anoFab, Integer valor, Integer quantEixos, TipoCarga tipoCarga) {
		super(placa, anoFab, valor);
		this.quantEixos = quantEixos;
		this.tipoCarga = tipoCarga;
	}

	public TipoCarga getTipoCarga() {
		return tipoCarga;
	}

	public Integer getQuantEixos() {
		return quantEixos;
	}

	public void setQuantEixos(Integer quantEixos) {
		this.quantEixos = quantEixos;
	}

	public void setTipoCarga(TipoCarga tipoCarga) {
		this.tipoCarga = tipoCarga;
	}

	public void incluir(Veiculos caminhao) {
    caminhoes.add(caminhao);
	}
	@Override
	public String toString() {
		return "Caminhoes [Placa= " + placa + ", Ano de Fabricação= " + anoFab + ", Valor= R$" + valor + ", Quantidade de Eixos= " + quantEixos + ", tipoCarga= " + tipoCarga + "]";
	}

}
