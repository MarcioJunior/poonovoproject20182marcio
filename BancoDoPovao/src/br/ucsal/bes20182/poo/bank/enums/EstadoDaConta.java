package br.ucsal.bes20182.poo.bank.enums;

public enum EstadoDaConta {
    ATIVA,BLOQUEADA,ENCERRADA;
}
