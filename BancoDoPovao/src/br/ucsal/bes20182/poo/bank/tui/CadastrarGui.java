package br.ucsal.bes20182.poo.bank.tui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import br.ucsal.bes20182.poo.bank.business.ClienteBO;
import br.ucsal.bes20182.poo.bank.domain.Cliente;
import br.ucsal.bes20182.poo.bank.enums.EstadoDaConta;
import java.awt.Toolkit;

public class CadastrarGui {
	static CadastrarGui window = new CadastrarGui();
	private JFrame frmBancoMyth;
	private static JTextField txtNome;
	private static JTextField txtTel;
	private static JTextField txtCpf;
	private static JTextField txtCep;
	private static JTextField txtRenda;
	public static boolean x = false;
	static Integer conta = 0;
	private static JTextField txtRg;
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					window.frmBancoMyth.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @wbp.parser.entryPoint
	 */
	public CadastrarGui() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| javax.swing.UnsupportedLookAndFeelException ex) {
			System.err.println(ex);
		}
		frmBancoMyth = new JFrame();
		frmBancoMyth.setIconImage(Toolkit.getDefaultToolkit().getImage(CadastrarGui.class.getResource("/br/ucsal/bes20182/poo/bank/img/BM.jpg")));
		frmBancoMyth.setTitle("BANCO MYTH");
		frmBancoMyth.getContentPane().setBackground(new Color(255, 102, 0));
		frmBancoMyth.setBounds(100, 100, 720, 649);
		frmBancoMyth.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmBancoMyth.getContentPane().setLayout(null);

		JLabel lblInformeSeuNome = new JLabel("Informe seu Nome:");
		lblInformeSeuNome.setForeground(Color.WHITE);
		lblInformeSeuNome.setBounds(102, 63, 130, 14);
		frmBancoMyth.getContentPane().add(lblInformeSeuNome);

		JLabel lblInformeSeuTelefone = new JLabel("Informe seu Telefone:");
		lblInformeSeuTelefone.setForeground(Color.WHITE);
		lblInformeSeuTelefone.setBounds(103, 130, 146, 14);
		frmBancoMyth.getContentPane().add(lblInformeSeuTelefone);

		txtNome = new JTextField();
		txtNome.setBounds(102, 88, 312, 31);
		frmBancoMyth.getContentPane().add(txtNome);
		txtNome.setColumns(10);

		txtTel = new JTextField();
		txtTel.setBounds(102, 155, 312, 31);
		frmBancoMyth.getContentPane().add(txtTel);
		txtTel.setColumns(10);

		txtCpf = new JTextField();
		txtCpf.setBounds(102, 229, 312, 32);
		frmBancoMyth.getContentPane().add(txtCpf);
		txtCpf.setColumns(10);

		JLabel lblInformeSeuCpf = new JLabel("Informe seu CPF:");
		lblInformeSeuCpf.setForeground(Color.WHITE);
		lblInformeSeuCpf.setBounds(102, 201, 104, 14);
		frmBancoMyth.getContentPane().add(lblInformeSeuCpf);

		txtCep = new JTextField();
		txtCep.setBounds(102, 304, 312, 31);
		frmBancoMyth.getContentPane().add(txtCep);
		txtCep.setColumns(10);

		JLabel lblInformeSeuCep = new JLabel("Informe seu Cep:");
		lblInformeSeuCep.setForeground(Color.WHITE);
		lblInformeSeuCep.setBounds(102, 279, 104, 14);
		frmBancoMyth.getContentPane().add(lblInformeSeuCep);

		JButton btnVoltar = new JButton("Voltar");
		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				window.frmBancoMyth.setVisible(false);
				MenuGui.main(null);
			}
		});
		btnVoltar.setBounds(590, 576, 89, 23);
		frmBancoMyth.getContentPane().add(btnVoltar);

		JButton btnAvanar = new JButton("Avan\u00E7ar");
		btnAvanar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				cadastrarCliente();
				
			}
		});
		btnAvanar.setBounds(476, 576, 89, 23);
		frmBancoMyth.getContentPane().add(btnAvanar);
		
		JLabel lblInformeSuaRenda = new JLabel("Informe sua Renda(SOMENTE NUMEROS INTEIROS):");
		lblInformeSuaRenda.setForeground(Color.WHITE);
		lblInformeSuaRenda.setBounds(102, 346, 312, 14);
		frmBancoMyth.getContentPane().add(lblInformeSuaRenda);
		
		txtRenda = new JTextField();
		txtRenda.setBounds(102, 372, 312, 31);
		frmBancoMyth.getContentPane().add(txtRenda);
		txtRenda.setColumns(10);
		
		JLabel lblInformeSeuRg = new JLabel("Informe seu Rg:");
		lblInformeSeuRg.setForeground(Color.WHITE);
		lblInformeSeuRg.setBounds(102, 414, 130, 14);
		frmBancoMyth.getContentPane().add(lblInformeSeuRg);
		
		txtRg = new JTextField();
		txtRg.setBounds(102, 439, 312, 31);
		frmBancoMyth.getContentPane().add(txtRg);
		txtRg.setColumns(10);
	}

	public static void cadastrarCliente() {
		long rendaSalarial = 0;

		String nome = txtNome.getText();

		String telefone = txtTel.getText();

		String cpf = txtCpf.getText();

		String cep = txtCep.getText();

		x = false;
		while (x == false) {
			try {
				
				rendaSalarial = Long.parseLong(txtRenda.getText());
				x = true;
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "EM RENDA, INFORME APENAS N�MEROS INTEIROS!", "ERRO",JOptionPane.ERROR_MESSAGE);
				rendaSalarial = Long.parseLong(JOptionPane.showInputDialog("INFORME A RENDA: ","APENAS NUMEROS INTEIROS"));
				x = true;
				
				
			}
		}

		
		String rg = txtRg.getText();

		JOptionPane.showMessageDialog(null, "CONTA ATIVA","SUCESSO",JOptionPane.INFORMATION_MESSAGE);
		EstadoDaConta estado = EstadoDaConta.ATIVA;


		Double saldo = (double) 0;

		Cliente cliente = new Cliente(nome, cpf, telefone, cep, rendaSalarial, rg, conta, estado, saldo);
		conta++;
		if (ClienteBO.validarCliente(cliente) == null)
			JOptionPane.showMessageDialog(null, "CLIENTE ADICIONADO NA LISTA!", cliente.getNome(), JOptionPane.INFORMATION_MESSAGE);
		else {
			JOptionPane.showMessageDialog(null, ClienteBO.validarCliente(cliente));
			
		}
        JOptionPane.showMessageDialog(null,"N�MERO DA SUA CONTA: " + (conta-1), "CONTA", JOptionPane.INFORMATION_MESSAGE);
        window.frmBancoMyth.setVisible(false);
        window = new CadastrarGui();
		MenuGui.main(null);
		
	}
}
