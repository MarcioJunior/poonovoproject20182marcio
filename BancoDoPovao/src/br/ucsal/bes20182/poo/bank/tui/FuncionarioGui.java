package br.ucsal.bes20182.poo.bank.tui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.ImageObserver;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import br.ucsal.bes20182.poo.bank.domain.Cliente;
import br.ucsal.bes20182.poo.bank.enums.EstadoDaConta;
import br.ucsal.bes20182.poo.bank.persistance.ClienteDAO;
import java.awt.Toolkit;

public class FuncionarioGui {

	static FuncionarioGui window = new FuncionarioGui();
	private JFrame frmBancoMyth;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					window.frmBancoMyth.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @wbp.parser.entryPoint
	 */
	public FuncionarioGui() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| javax.swing.UnsupportedLookAndFeelException ex) {
			System.err.println(ex);
		}
		frmBancoMyth = new JFrame();
		frmBancoMyth.setIconImage(Toolkit.getDefaultToolkit().getImage(FuncionarioGui.class.getResource("/br/ucsal/bes20182/poo/bank/img/BM.jpg")));
		frmBancoMyth.setTitle("BANCO MYTH");
		frmBancoMyth.getContentPane().setBackground(new Color(255, 102, 0));
		frmBancoMyth.setBounds(100, 100, 450, 300);
		frmBancoMyth.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmBancoMyth.getContentPane().setLayout(null);

		JLabel lblAcessoRestrito = new JLabel("\u00C1REA RESTRITA(FUNCION\u00C1RIOS)");
		lblAcessoRestrito.setFont(new Font("Monospaced", Font.PLAIN, 15));
		lblAcessoRestrito.setForeground(Color.WHITE);
		lblAcessoRestrito.setBounds(96, 11, 274, 14);
		frmBancoMyth.getContentPane().add(lblAcessoRestrito);

		JButton btnBloquearConta = new JButton("Bloquear Conta");
		btnBloquearConta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				bloquearCliente();
			}
		});
		btnBloquearConta.setBounds(46, 88, 142, 23);
		frmBancoMyth.getContentPane().add(btnBloquearConta);

		JButton btnNewButton = new JButton("Desbloquear Conta");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				desbloquearCliente();
			}
		});
		btnNewButton.setBounds(46, 122, 142, 23);
		frmBancoMyth.getContentPane().add(btnNewButton);

		JButton btnEncerrarCliente = new JButton("Encerrar Cliente");
		btnEncerrarCliente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				encerrarCliente();
			}
		});
		btnEncerrarCliente.setBounds(46, 156, 142, 23);
		frmBancoMyth.getContentPane().add(btnEncerrarCliente);

		JButton btnAdicionarSaldo = new JButton("Adicionar Saldo");
		btnAdicionarSaldo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				adicionarSaldo();
			}
		});
		btnAdicionarSaldo.setBounds(46, 190, 142, 23);
		frmBancoMyth.getContentPane().add(btnAdicionarSaldo);

		JButton btnSair = new JButton("Encerrar");
		btnSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnSair.setBounds(335, 227, 89, 23);
		frmBancoMyth.getContentPane().add(btnSair);

		JLabel label = new JLabel("");
		label.setBounds(267, 74, 130, 113);

		ImageIcon ima = new ImageIcon(FuncionarioGui.class.getResource("/br/ucsal/bes20182/poo/bank/img/768px-Emoji_u1f510.svg.png"));
		Image imag = ima.getImage().getScaledInstance(label.getWidth(), label.getHeight(), ImageObserver.WIDTH);
		label.setIcon(new ImageIcon(imag));
		frmBancoMyth.getContentPane().add(label);

		JButton btnMenu = new JButton("Menu");
		btnMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				window.frmBancoMyth.setVisible(false);
				MenuGui.main(null);
			}
		});
		btnMenu.setBounds(240, 227, 89, 23);
		frmBancoMyth.getContentPane().add(btnMenu);

	}

	public static void adicionarSaldo() {
		List<Cliente> clientes = ClienteDAO.todosClientes();
		if (clientes.size() == 0) {
			JOptionPane.showMessageDialog(null, "CLIENTES N�O CADASTRADOS!","ERRO",JOptionPane.ERROR_MESSAGE);

		} else {

			for (Cliente cliente : clientes) {
				if (cliente.getEstado() == EstadoDaConta.ATIVA) {
					JOptionPane.showMessageDialog(null, "Clientes e Saldos: " + "\nNome: " + cliente.getNome()
							+ "\nSaldo: " + cliente.getSaldo() + "\nConta: " + cliente.getConta(),"DADOS",JOptionPane.INFORMATION_MESSAGE);

				}
			}

			int escolhaConta = Integer.parseInt(
					JOptionPane.showInputDialog(("Informe pela conta qual cliente deseja modificar o saldo: ")));
			for (Cliente cliente2 : clientes) {
				if (cliente2.getConta() == escolhaConta && cliente2.getEstado() == EstadoDaConta.ATIVA) {
					cliente2.setSaldo(Double.parseDouble(JOptionPane
							.showInputDialog(("Informe o valor a ser Aplicado: "))));
					JOptionPane.showMessageDialog(null,"VALOR APLICADO ", "SUCESSO!",JOptionPane.INFORMATION_MESSAGE);
				}
			}
		}
	}

	public static void desbloquearCliente() {
		List<Cliente> clientes = ClienteDAO.todosClientes();
		if (clientes.size() == 0) {
			JOptionPane.showMessageDialog(null, "CLIENTES N�O CADASTRADOS!", "ERRO",JOptionPane.ERROR_MESSAGE);

		} else {
			int escolhaDesbloquear = Integer
					.parseInt(JOptionPane.showInputDialog(("Informe pela conta qual cliente deseja desbloquear: ")));
			for (Cliente cliente : clientes) {
				if (escolhaDesbloquear == cliente.getConta() && cliente.getEstado() == EstadoDaConta.BLOQUEADA) {
					cliente.setEstado(EstadoDaConta.ATIVA);
					JOptionPane.showMessageDialog(null, "O cliente " + cliente.getNome() + " foi desbloqueado!", "SUCESSO", JOptionPane.INFORMATION_MESSAGE);

				} else {
					JOptionPane.showMessageDialog(null,
							"N�o h� cliente com n�mero da conta correspondente que possa ser bloqueado.", "ERRO", JOptionPane.ERROR_MESSAGE);
				}
			}

		}

	}

	public static void encerrarCliente() {
		List<Cliente> clientes = ClienteDAO.todosClientes();
		if (clientes.size() == 0) {
			JOptionPane.showMessageDialog(null, "CLIENTES N�O CADASTRADOS!", "ERRO",JOptionPane.ERROR_MESSAGE);

		} else {

			int escolhaEncerrar = Integer.parseInt(JOptionPane
					.showInputDialog(("Informe o n�mero da conta referente ao cliente que desej�s encerrar: ")));

			for (Cliente cliente : clientes) {
				if (escolhaEncerrar == cliente.getConta() && cliente.getEstado() == EstadoDaConta.ATIVA
						|| cliente.getEstado() == EstadoDaConta.BLOQUEADA && cliente.getSaldo() < 1) {
					cliente.setEstado(EstadoDaConta.ENCERRADA);
					JOptionPane.showMessageDialog(null, "O cliente " + cliente.getNome() + " foi encerrado!","SUCESSO",JOptionPane.INFORMATION_MESSAGE);

				} else if (cliente.getSaldo() > 1) {
					JOptionPane.showMessageDialog(null,
							"S� � poss�vel encerrar contas sem saldo, favor realize o saque dos " + cliente.getSaldo()
									+ "R$ restantes!");

				} else {
					JOptionPane.showMessageDialog(null,
							"N�o h� cliente com n�mero da conta correspondente que possa ser encerrado.","ERRO",JOptionPane.ERROR_MESSAGE);

				}
			}

		}

	}

	public static void bloquearCliente() {
		List<Cliente> clientes = ClienteDAO.todosClientes();
		if (clientes.size() == 0) {
			JOptionPane.showMessageDialog(null, "CLIENTES N�O CADASTRADOS!","ERRO",JOptionPane.ERROR_MESSAGE);

		} else {
			int escolhaBloquear = Integer.parseInt(JOptionPane
					.showInputDialog(("Informe o n�mero da conta referente ao cliente que desej�s bloquear: ")));

			for (Cliente cliente : clientes) {
				if (escolhaBloquear == cliente.getConta()) {
					cliente.setEstado(EstadoDaConta.BLOQUEADA);
					JOptionPane.showMessageDialog(null, "O cliente " + cliente.getNome() + " foi bloqueado!","SUCESSO",JOptionPane.INFORMATION_MESSAGE);

				} else {
					JOptionPane.showMessageDialog(null,
							"N�o h� cliente com n�mero da conta correspondente que possa ser bloqueado.","ERRO",JOptionPane.ERROR_MESSAGE);

				}
			}
		}
	}
}
