package br.ucsal.bes20182.poo.bank.tui;

import java.util.List;
import java.util.Scanner;

import javax.swing.JOptionPane;

import br.ucsal.bes20182.poo.bank.domain.Cliente;
import br.ucsal.bes20182.poo.bank.enums.EstadoDaConta;
import br.ucsal.bes20182.poo.bank.persistance.ClienteDAO;

public class MenuTui {
	public static final String SENHA_FUNCIONARIO = "20182";
	static Scanner sc = new Scanner(System.in);
	static Integer conta = 0, tentativa = 0;
	public static boolean x = false;

	public static void menu() {
		System.out.println("");
		System.out.println("(0) TRANSAÇÕES \n(1) ABRIR CONTA \n(2) CONTAS \n(3) ACESSO FUNCIONÁRIO \n(4) SAIR ");
		int escolha = sc.nextInt();
		sc.nextLine();

		switch (escolha) {
		case 0:
			Transacoes.transacoes();
			break;
		case 1:
			Cadastrar.cadastrarCliente();

			break;

		case 2:
			listarCliente();
			break;

		case 3:
			Funcionario.acessoFuncionario();
			break;
		}
	}

	public static void listarCliente() {

		List<Cliente> clientes = ClienteDAO.todosClientes();
		if (clientes.size() == 0) {
			System.out.println("SEM CONTAS DISPONÍVEIS!");
			menu();
		}
		for (Cliente cliente : clientes) {
			if (cliente.getEstado() == EstadoDaConta.BLOQUEADA)
				System.out.println("CONTA DO CLIENTE: " + cliente.getConta() + ": " + cliente.getNome()
						+ " BLOQUEADA COM SUCESSO!");

			else if (cliente.getEstado() == EstadoDaConta.ENCERRADA)
				System.out
						.println("A Conta: " + cliente.getConta() + " foi encerrada com sucesso!" + cliente.getConta());
			else {
				JOptionPane.showMessageDialog(null,
						" CONTA " + cliente.getConta() + "Nome: " + cliente.getNome() + "Telefone: "
								+ cliente.getTelefone() + "CPF: " + cliente.getCpf() + "CEP: " + cliente.getCep()
								+ "Renda: " + cliente.getRendaSalarial() + "RG: " + cliente.getRg() + "Conta: "
								+ cliente.getConta() + "Saldo: " + cliente.getSaldo()+ "Estado da Conta: " + cliente.getEstado());
				System.out.println();

				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();

				System.out.println();
			}
		}
		menu();
	}

}
